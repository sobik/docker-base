#!/bin/bash

registry=registry.budgetbakers.com:5000
script_home=$(dirname $(readlink -f $0))
images="base/ubuntu 
	base/java 
      	base/python 
	tools/gaucho
	services/couchbase-server"

for image in $images
do
	if [ ! -d $script_home/$image ];
	then 
          echo Directory $script_home/$image  does not exit.
	  exit 1
	fi
done

for imageDir in $images
do
	echo Building $imageDir
	cd $script_home/$imageDir || exit 2
	image=$(basename $imageDir)
        docker build -t $registry/$image . || exit 3
	echo Building image $image done.
done
